#include<stdio.h>
int calc_profit(int price);
int revenue(int price);
int attendance(int price);
int cost(int price);
void display();

int attendance(int price){
	int atten;
	atten = (120-(price-15)/5*20);
	return atten;
	
}

int revenue(int price){
	return price*attendance(price);
}

int cost(int price){
	const int x = 500;
	return x+3*attendance(price);
}
int calc_profit(int price){
	return revenue(price) - cost(price);
}

void display(){
	int i=5,profit;
	for(i=5;i<=50;i+=5){
		profit=calc_profit(i);
		printf("Rs.%d \t Rs.%d\n",i,profit);
	}
}
int main()
{
	printf("Table displays the relationship between profit and ticket price\n");
	printf("Price \t profit\n");
	printf("The highest profit is at the ticket price of Rs.25\n");
	display();
	return 0;
}

